# Backend
## NodeJS
- Possibilita o non block io:
    - Dentro do node temos um controle sobre assincronismo.
    - Exemplo:
        - Podemos enviar um e-mail e, enquanto este é enviado, podemos executar outras tarefas sem nenhum problema.
    - Não temos que esperar uma operação terminar, para que a próxima linha possa ser executada.
- Streams:
    - Consumo cadenciado de dados.
        - Ex: ler um arquivo linha por linha e já começar a trabalhar nele antes de ler o arquivo por completo.
- Worker threads:
    - Nos permite trabalhar como todos os cores de um processador para executar ações diferentes dentro de um servidor node.
- Tudo isso irá nos provisionar mais performace no nosso backend.

## Iniciando o servidor
- npm init -y